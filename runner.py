from __future__ import absolute_import
from __future__ import print_function
import threading

import os
import sys
import optparse
import random
import statistics as stat

# we need to import python modules from the $SUMO_HOME/tools directory
if 'SUMO_HOME' in os.environ:
    tools = os.path.join(os.environ['SUMO_HOME'], 'tools')
    sys.path.append(tools)
else:
    sys.exit("please declare environment variable 'SUMO_HOME'")

from sumolib import checkBinary  # noqa
import traci  # noqa


def generate_routefile():
    random.seed(42)  # make tests reproducible
    N = 3600  # number of time steps
    # demand per second from different directions
    pWE = 1. / 5
    pEW = 1. / 5
    pNS = 1. / 12
    pSN = 1. / 12
    with open("data/cross.rou.xml", "w") as routes:
        print("""<routes>
        <vType id="typeWE" accel="0.8" decel="4.5" sigma="0.5" length="5" minGap="2.5" maxSpeed="16.67" guiShape="passenger"/>
        <vType id="typeNS" accel="0.8" decel="4.5" sigma="0.5" length="5" minGap="3" maxSpeed="25" guiShape="bus"/>

        <route id="right" edges="51o 1i 2o 52i" />
        <route id="left" edges="52o 2i 1o 51i" />
        <route id="down" edges="54o 4i 3o 53i" />
        <route id="up" edges="53o 3i 4o 54i" />""", file=routes)
        vehNr = 0
        for i in range(N):
            if random.uniform(0, 1) < pWE:
                print('    <vehicle id="right_%i" type="typeWE" route="right" depart="%i" />' % (
                    vehNr, i), file=routes)
                vehNr += 1
            if random.uniform(0, 1) < pEW:
                print('    <vehicle id="left_%i" type="typeWE" route="left" depart="%i" />' % (
                    vehNr, i), file=routes)
                vehNr += 1
            if random.uniform(0, 1) < pNS:
                print('    <vehicle id="down_%i" type="typeNS" route="down" depart="%i" color="1,0,0"/>' % (
                    vehNr, i), file=routes)
                vehNr += 1
            if random.uniform(0, 1) < pSN:
                print('    <vehicle id="up_%i" type="typeNS" route="up" depart="%i" color="1,0,0"/>' % (
                    vehNr, i), file=routes)
                vehNr += 1
        print("</routes>", file=routes)


def get_lane_density(traci, lane_name):
    car_length = 5
    return car_length * traci.lanearea.getLastStepVehicleNumber(lane_name)/traci.lanearea.getLength(lane_name)


def get_average_world_density(traci, lane_names):
    return stat.mean(map(lambda name: get_lane_density(traci, name), [lane for lane in lane_names]))


def old_algorithm_step(traci, steps):
    traci.simulationStep()

    avg_density = get_average_world_density(traci, ["0", "1", "2", "3"])

    return 'Old: ' + '{:.2f}'.format(avg_density*100).zfill(5) + ' %'


ver_cars = hor_cars = 0


def new_algorithm_step(traci, steps):
    global ver_cars
    global hor_cars

    traci.simulationStep()

    ver_cars += traci.lanearea.getLastStepVehicleNumber("0")
    ver_cars += traci.lanearea.getLastStepVehicleNumber("3")

    hor_cars += traci.lanearea.getLastStepVehicleNumber("1")
    hor_cars += traci.lanearea.getLastStepVehicleNumber("2")

    tl_change_rate = 5000  # every 50 seconds
    if steps % tl_change_rate == 0 and (hor_cars > 0 or ver_cars > 0):
        res = traci.trafficlight.getCompleteRedYellowGreenDefinition("0")[0]

        max_duration = 60  # whole traffic ligth wait time in seconds save yellow color

        ver_duration = max_duration * ver_cars / (hor_cars + ver_cars)
        hor_duration = max_duration * hor_cars / (hor_cars + ver_cars)

        res.getPhases()[0].duration = ver_duration  # vertical lane
        res.getPhases()[2].duration = hor_duration  # horizontal lane

        traci.trafficlight.setCompleteRedYellowGreenDefinition("0", res)
        ver_cars = hor_cars = 0

    avg_density = get_average_world_density(traci, ["0", "1", "2", "3"])
    res = traci.trafficlight.getCompleteRedYellowGreenDefinition("0")[0]

    return 'New: ' + '{:.2f}'.format(avg_density*100).zfill(5) + ' %; Time: ' + str(res.getPhases()[2].duration) + ', ' + str(res.getPhases()[0].duration)


def run_simulation(traci_new, traci_old):
    steps = 0

    while traci_new.simulation.getMinExpectedNumber() > 0 or traci_old.simulation.getMinExpectedNumber() > 0:

        new_ret = new_algorithm_step(traci_new, steps)
        old_ret = old_algorithm_step(traci_old, steps)

        print(' > ' + new_ret + ' - ' + old_ret, end='\r')

        steps += 1

    traci.close()
    sys.stdout.flush()


def start():
    sumoBinary = checkBinary('sumo-gui')

    generate_routefile()

    cmd_args = [sumoBinary, "-c", "data/cross.sumocfg", "--tripinfo-output", "tripinfo.xml",
                "--start", "true", "--step-length", "0.05"]

    traci.start(cmd_args + ["--window-pos", "0,50", "--window-size", "650, 650"],
                label='old_sim')
    traci.start(cmd_args + ["--window-pos", "700,50", "--window-size", "650, 650"],
                label='new_sim')

    new_sim_connection = traci.getConnection("new_sim")
    old_sim_connection = traci.getConnection("old_sim")

    run_simulation(new_sim_connection, old_sim_connection)


if __name__ == '__main__':
    start()
